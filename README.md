# SAST Scanner Service

This project demonstrates using the Semgrep lanugage server, `semgrep lsp`, to implement a SAST scanning service.

Currently, there is no server, only a command line demo tool to demonstrate.

setup
```sh
asdf install
python -mvenv .venv
. .venv/bin/activate
pip3 install semgrep
```

and run
```sh
go run ./cmd/cli -c .../semgrep_rules.yml .../source1.java .../source2.go
```


## overview
`semgrep lsp`, like most language servers, communicates using JSON-PRC over `stdin` and `stdout` in accordance with the [LSP spec](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/).

`semgrep lsp` assumes that files are _on disk_ so in order to scan a file, it must be written to disk.  We use `/tmp` for that purpose.

There is no "scan this file" method in LSP. Instead, certain notifications sent by the client _trigger_ a scan. Scan results are asynchronously [published](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_publishDiagnostics) by the server.  Synchronizing the file written with its scan results is accomplished with the following sequence of steps:
1. write the file to `/tmp`
2. notify the server that a workspace (`/tmp`) has been added
3. request a scan of the workspace
4. notify the server that the workspace was removed
6. wait for progress notifications indicating that the scan was performed
7. fetch the last diagnostics sent by the server


[`SemgrepLSP`](/semgrep_lsp/semgrep_lsp.go) encapsulates the process control and abstracts some of the messaging operations.

[`Scanner`](/scanner/scanner.go) simplifies synchronization and scanning.
